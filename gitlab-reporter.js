'use strict';

const md5 = require('md5');

module.exports = function(errorsCollection) {

  let output = [];

  errorsCollection.forEach((errors) => {
    let filename = errors.getFilename();
    errors.getErrorList().forEach((e) => {

      let message = e.message
          .replace(/\n/g, '\\n')
          .replace(/\r/g, '\\r')
          .replace(/\t/g, '\\t');

      output.push({
        type: 'issue',
        categories: ['Style', 'Javascript'],
        fingerprint: md5(filename + e.message + e.line),
        description: message,
        location: {
          path: filename,
          lines: {
            begin: e.line,
            end: e.line
          }
        }
      });
    });
  });

  console.log(JSON.stringify(output));
};
