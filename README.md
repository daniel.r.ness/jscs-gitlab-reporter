# jscs-gitlab-reporter

## Install

```
npm install jscs-gitlab-reporter --save-dev
```

## Usage

```
jscs . --reporter node_modules/jscs-gitlab-reporter/jscs-gitlab-reporter.js
```

Output will be written to STDOUT 

## License
Licensed under the MIT license. Copyright (c) 2020 Daniel Ness

